package artemiygreg.mynotes.broadcast_receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import artemiygreg.mynotes.service.NotificationService;

/**
 * Created by artem_mobile_dev on 21.11.2015.
 */
public class ReminderBroadcastReceiver extends BroadcastReceiver {
    private AlarmManager alarmManager;
    private Context context;
    public static final String ACTION_UPDATE_REMINDER = "action.receiver.update.reminder";

    public ReminderBroadcastReceiver(){

    }

    public ReminderBroadcastReceiver(Context context){
        this.context = context;
        alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(ACTION_UPDATE_REMINDER)){
            NotificationService notificationService = NotificationService.getInstance();
            notificationService.sendNotification("Напоминание", "Блаблаблабла");
        }
    }

    public void setAlarmManager(long timestamp){
        Intent intent = new Intent(context, ReminderBroadcastReceiver.class);
        intent.setAction(ACTION_UPDATE_REMINDER);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int)System.currentTimeMillis(), intent, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, timestamp, pendingIntent);
    }
}