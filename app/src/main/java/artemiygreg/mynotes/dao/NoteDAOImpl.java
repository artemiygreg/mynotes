package artemiygreg.mynotes.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import artemiygreg.mynotes.model.Note;

/**
 * Created by Admin on 23.02.15.
 */
public class NoteDAOImpl extends SQLiteOpenHelper implements NoteDAO {
    public static final long SQL_ERROR = -1;
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "db_my_notes";
    private static final String TABLE_NAME_NOTES = "notes";
    private static final String ID = "id";
    public static final String TITLE_NOTE = "title_note";
    public static final String TEXT_NOTE = "text_note";
    public static final String DATE_CREATED = "date_created";
    public static final String DATE_EDITED = "date_edited";
    public static final String DATE_REMINDER = "date_reminder";
    private static final String SORT_BY_ID = ID + " desc";
    private static final String SORT_BY_TITLE_NOTE = TITLE_NOTE + " desc";
    private static final String SORT_BY_TEXT_NOTE = TEXT_NOTE + " desc";
    private static final String SORT_BY_DATE_CREATED = DATE_CREATED + " desc";
    private static final String SORT_BY_DATE_EDITED = DATE_EDITED + " desc";
    private static final String SORT_BY_DATE_REMINDER = DATE_REMINDER + " desc";
    private static final String CREATE_TABLE_NOTES = "create table " + TABLE_NAME_NOTES + " ( " +
            ID + " integer primary key autoincrement, " +
            TITLE_NOTE + " text, " +
            TEXT_NOTE + " text, " +
            DATE_CREATED + " integer, " +
            DATE_EDITED + " integer (0), " +
            DATE_REMINDER + " integer (0))";
    public static final String DROP_TABLE_NOTES = "drop table if exists " + TABLE_NAME_NOTES;

    public NoteDAOImpl(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_NOTES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_NOTES);
        onCreate(db);
    }

    @Override
    public List<Note> getListNotes(SQLiteDatabase sqLiteDatabase) {
        List<Note> listNotes = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_NAME_NOTES, null);
        if(cursor.moveToFirst()){
            int indexId = cursor.getColumnIndex(ID);
            int indexTitle = cursor.getColumnIndex(TITLE_NOTE);
            int indexText = cursor.getColumnIndex(TEXT_NOTE);
            int indexDateCreated = cursor.getColumnIndex(DATE_CREATED);
            int indexDateEdited = cursor.getColumnIndex(DATE_EDITED);
            int indexDateReminder = cursor.getColumnIndex(DATE_REMINDER);
            do {
                Note note = new Note();
                note.setId(cursor.getLong(indexId));
                note.setTitleNotes(cursor.getString(indexTitle));
                note.setTextNotes(cursor.getString(indexText));
                note.setDateCreated(cursor.getLong(indexDateCreated));
                note.setDateEdited(cursor.getLong(indexDateEdited));
                note.setDateReminder(cursor.getLong(indexDateReminder));
                note.setEdited(cursor.getLong(indexDateEdited) > 0);

                listNotes.add(note);
            }
            while (cursor.moveToNext());
        }
        cursor.close();

        return listNotes;
    }

    @Override
    public long saveNotes(SQLiteDatabase sqLiteDatabase, String nameNotes, String textNotes, long dateCreated, long dateEdited, long dateReminder) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE_NOTE, nameNotes);
        contentValues.put(TEXT_NOTE, textNotes);
        contentValues.put(DATE_CREATED, dateCreated);
        contentValues.put(DATE_EDITED, dateEdited);
        contentValues.put(DATE_REMINDER, dateReminder);

        return sqLiteDatabase.insert(TABLE_NAME_NOTES, null, contentValues);
    }

    @Override
    public SQLiteDatabase getDatabaseRead() {
        return getReadableDatabase();
    }

    @Override
    public SQLiteDatabase getDatabaseWrite() {
        return getWritableDatabase();
    }



    /**
     * @return number edited recorder
     **/
    @Override
    public int editNoteById(SQLiteDatabase sqLiteDatabase, String nameNotes, String textNotes, long id,
                             long dateEdited, long dateReminder) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE_NOTE, nameNotes);
        contentValues.put(TEXT_NOTE, textNotes);
        contentValues.put(DATE_EDITED, dateEdited);
        contentValues.put(DATE_REMINDER, dateReminder);

        return sqLiteDatabase.update(TABLE_NAME_NOTES, contentValues, ID + "=" + id, null);
    }

    @Override
    public int deleteNoteById(SQLiteDatabase sqLiteDatabase, long id) {
        return sqLiteDatabase.delete(TABLE_NAME_NOTES, ID + "='" + id + "'", null);
    }
}
