package artemiygreg.mynotes.dao;

import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import artemiygreg.mynotes.model.Note;
/**
 * Created by Admin on 23.02.15.
 */
public interface NoteDAO {


    List<Note> getListNotes(SQLiteDatabase sqLiteDatabase);
    long saveNotes(SQLiteDatabase sqLiteDatabase, String nameNotes, String textNotes,
                          long dateCreated, long dateEdited, long dateReminder);
    SQLiteDatabase getDatabaseRead();
    SQLiteDatabase getDatabaseWrite();
    int editNoteById(SQLiteDatabase sqLiteDatabase, String nameNotes, String textNotes,
                      long id, long dateEdited, long dateReminder);

    int deleteNoteById(SQLiteDatabase sqLiteDatabase, long id);
}
