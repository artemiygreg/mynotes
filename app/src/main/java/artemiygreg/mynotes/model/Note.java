package artemiygreg.mynotes.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 23.02.15.
 */
public class Note implements Parcelable {
    private long id;
    private String titleNotes;
    private String textNotes;
    private long dateCreated;
    private long dateEdited;
    private boolean edited = false;
    private long dateReminder;

    public Note(){

    }

    private Note(Parcel source){
        this.id = source.readLong();
        this.dateCreated = source.readLong();
        this.dateEdited = source.readLong();
        this.dateReminder = source.readLong();
        this.titleNotes = source.readString();
        this.textNotes = source.readString();
        this.edited = source.readInt() == 1;

        source.recycle();
    }

    public Note(long id, String titleNotes, String textNotes, long dateCreated, long dateEdited, long dateReminder){
        this.id = id;
        this.titleNotes = titleNotes;
        this.textNotes = textNotes;
        this.dateCreated = dateCreated;
        this.dateEdited = dateEdited;
        this.dateReminder = dateReminder;
    }

    public void setId(long id){
        this.id = id;
    }

    public long getId(){
        return this.id;
    }

    public void setDateCreated(long dateCreated){
        this.dateCreated = dateCreated;
    }

    public long getDateCreated(){
        return this.dateCreated;
    }

    public void setDateEdited(long dateEdited){
        this.dateEdited = dateEdited;
    }

    public long getDateEdited(){
        return this.dateEdited;
    }

    public void setTitleNotes(String titleNotes){
        this.titleNotes = titleNotes;
    }

    public String getTitleNotes(){
        return this.titleNotes;
    }

    public void setTextNotes(String textNotes){
        this.textNotes = textNotes;
    }

    public String getTextNotes(){
        return this.textNotes;
    }

    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }

    public long getDateReminder() {
        return dateReminder;
    }

    public void setDateReminder(long dateReminder) {
        this.dateReminder = dateReminder;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator(){
        @Override
        public Note createFromParcel(Parcel source) {
            return new Note(source);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[0];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(dateCreated);
        dest.writeLong(dateEdited);
        dest.writeLong(dateReminder);
        dest.writeString(titleNotes);
        dest.writeString(textNotes);
        dest.writeInt(edited ? 1 : 0);
//        dest.recycle();
    }
}
