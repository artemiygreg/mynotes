package artemiygreg.mynotes.mvp.presenters;

import android.support.annotation.NonNull;

import artemiygreg.mynotes.R;
import artemiygreg.mynotes.mvp.models.MyNotesModel;
import artemiygreg.mynotes.mvp.views.MyListNotesView;
import rx.Subscription;

/**
 * Created by artem_mobile_dev on 02.01.2016.
 */
public class MyNotesPresenter extends BasePresenter<MyListNotesView> {

    @NonNull
    private final MyNotesModel myNotesModel;


    public MyNotesPresenter(@NonNull MyNotesModel myNotesModel){
        this.myNotesModel = myNotesModel;
    }

    public void loadData() {
        {
            final MyListNotesView view = view();
            if (view != null) {
                view.showLoading();
            }
        }
        Subscription subscription = myNotesModel.getObservableListNotes()
                .subscribe(listNotes -> {
                        final MyListNotesView view = view();

                        if(view != null){
                            view.hideLoading();
                            if(listNotes == null || listNotes.isEmpty()){
                                view.showNotNotes();
                            }
                            else {
                                view.showContent(listNotes);
                            }
                        }
                    },
                    error -> {
                        final MyListNotesView view = view();

                        if(view != null){
                            view.hideLoading();
                            view.showError(error);
                        }
                    });
        unsubscribeOnUnbindView(subscription);
    }

    public void deleteNoteById(long id, int position) {
        Subscription subscription = myNotesModel.deleteNoteById(id)
                .subscribe(result -> {
                            if (!result) {
                                final MyListNotesView view = view();
                                if(view != null){
                                    view.showError(R.string.failed_to_remove_note);
                                }
                            } else {
                                final MyListNotesView view = view();
                                if(view != null){
                                    view.noteSuccessfullyDeleted(position);
                                }
                            }
                        },
                        throwable -> {
                            final MyListNotesView view = view();
                            if(view != null){
                                view.showError(R.string.failed_to_remove_note);
                            }
                        });
        unsubscribeOnUnbindView(subscription);
    }
}
