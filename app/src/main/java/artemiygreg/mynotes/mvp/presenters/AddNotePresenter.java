package artemiygreg.mynotes.mvp.presenters;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import artemiygreg.mynotes.model.Note;
import artemiygreg.mynotes.mvp.models.MyNotesModel;
import artemiygreg.mynotes.mvp.views.AddOrEditNoteView;
import rx.Subscription;

/**
 * Created by artem_mobile_dev on 07.01.2016.
 */
public class AddNotePresenter extends BasePresenter<AddOrEditNoteView> {

    @NonNull
    protected final MyNotesModel myNotesModel;


    public AddNotePresenter(@NonNull MyNotesModel myNotesModel){
        this.myNotesModel = myNotesModel;
    }

    public void saveNote(@NonNull Note note) {
        {
            final AddOrEditNoteView view = view();
            if (view != null) {
                view.showLoading();
            }
        }
        Subscription subscription = myNotesModel.saveNote(note.getTitleNotes(),
                note.getTextNotes(), note.getDateCreated(), note.getDateEdited(), note.getDateReminder())
                .subscribe(noteIsSuccessSaved -> {
                            final AddOrEditNoteView view = view();

                            if (view != null) {
                                view.hideLoading();
                                if (!noteIsSuccessSaved) {
                                    view.showError(null, new Exception("The note was not saved"));
                                } else {
                                    view.noteSavedSuccessfully();
                                }
                            }
                        },
                        error -> {
                            final AddOrEditNoteView view = view();

                            if (view != null) {
                                view.hideLoading();
                                view.showError(null, error);
                            }
                        });
        unsubscribeOnUnbindView(subscription);
    }

    public void onCreate(@Nullable Bundle saveInstance, @Nullable Intent intent, long dateCreatedMs){
        final AddOrEditNoteView view = view();

        if (view != null) {
            view.onCreated(saveInstance, intent, dateCreatedMs);
        }
    }

    @Override
    public void unbindView(@NonNull AddOrEditNoteView view) {
        super.unbindView(view);
        myNotesModel.closeSQLite();
    }
}
