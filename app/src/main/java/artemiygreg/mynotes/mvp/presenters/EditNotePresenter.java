package artemiygreg.mynotes.mvp.presenters;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import artemiygreg.mynotes.model.Note;
import artemiygreg.mynotes.mvp.models.MyNotesModel;
import artemiygreg.mynotes.mvp.views.AddOrEditNoteView;
import artemiygreg.mynotes.service.DateService;
import rx.Subscription;

/**
 * Created by artem_mobile_dev on 25.01.2016.
 */
public class EditNotePresenter extends AddNotePresenter {

    private Note note;

    public EditNotePresenter(MyNotesModel myNotesModel) {
        super(myNotesModel);
    }

    public void editNoteById(@NonNull Note note){
        {
            final AddOrEditNoteView view = view();
            if (view != null) {
                view.showLoading();
            }
        }
        Subscription subscription = myNotesModel.editNoteById(note.getId(), note.getTitleNotes(),
                note.getTextNotes(), note.getDateEdited(), note.getDateReminder())
                .subscribe(noteIsSuccessSaved -> {
                            final AddOrEditNoteView view = view();

                            if (view != null) {
                                view.hideLoading();
                                if (!noteIsSuccessSaved) {
                                    view.showError(null, new Exception("The note was not edited"));
                                } else {
                                    view.noteSavedSuccessfully();
                                }
                            }
                        },
                        error -> {
                            final AddOrEditNoteView view = view();

                            if (view != null) {
                                view.hideLoading();
                                view.showError(null, error);
                            }
                        });
        unsubscribeOnUnbindView(subscription);
    }

    @Override
    public void onCreate(@Nullable Bundle saveInstance, @Nullable Intent intent, long dateCreatedMs) {
        super.onCreate(saveInstance, intent, dateCreatedMs);
        if(intent != null){
            note = intent.getParcelableExtra("note");
        }
    }

    public void editNoteById(String name, String text){
        {
            final AddOrEditNoteView view = view();
            if (view != null) {
                view.showLoading();
            }
        }
        Subscription subscription = myNotesModel.editNoteById(note.getId(), name, text,
                DateService.getCurrentTimeMillis(), note.getDateReminder())
                .subscribe(noteIsSuccessSaved -> {
                            final AddOrEditNoteView view = view();

                            if (view != null) {
                                view.hideLoading();
                                if (!noteIsSuccessSaved) {
                                    view.showError(null, new Exception("The note was not edited"));
                                } else {
                                    view.noteSavedSuccessfully();
                                }
                            }
                        },
                        error -> {
                            final AddOrEditNoteView view = view();

                            if (view != null) {
                                view.hideLoading();
                                view.showError(null, error);
                            }
                        });
        unsubscribeOnUnbindView(subscription);
    }
}
