package artemiygreg.mynotes.mvp.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import artemiygreg.mynotes.model.Note;

/**
 * Created by artem_mobile_dev on 07.01.2016.
 */
public interface AddOrEditNoteView {

    void onCreated(@Nullable Bundle saveInstance, @Nullable Intent intent, long dateCreatedMs);

    void showLoading();

    void hideLoading();

    void showError(@Nullable String message, @NonNull Throwable throwable);

    void showNoteContent(@NonNull Note note);

    void noteSavedSuccessfully();
}
