package artemiygreg.mynotes.mvp.views;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import java.util.List;

import artemiygreg.mynotes.model.Note;

/**
 * Created by artem_mobile_dev on 02.01.2016.
 */
public interface MyListNotesView {

    void showLoading();

    void hideLoading();

    void showContent(@NonNull List<Note> listNotes);

    void showError(String message);

    void showError(@StringRes int resId);

    void showError(@NonNull Throwable error);

    void showNotNotes();

    void noteSuccessfullyDeleted(int position);
}
