package artemiygreg.mynotes.mvp.models;

import java.util.List;

import artemiygreg.mynotes.dao.NoteDAOImpl;
import artemiygreg.mynotes.model.Note;
import artemiygreg.mynotes.service.NotesService;
import artemiygreg.mynotes.service.NotesServiceImpl;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by artem_mobile_dev on 02.01.2016.
 */
public class MyNotesModel {
    private NotesService notesService;

    public MyNotesModel(NotesServiceImpl notesServiceImpl){
        notesService = notesServiceImpl;
    }

    public Observable<List<Note>> getObservableListNotes(){
        return Observable.fromCallable(notesService::getListNotes)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread());
    }

    public Observable<Boolean> saveNote(String nameNotes, String textNotes, long dateCreated, long dateEdited, long dateReminder){
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    long row = notesService.saveNotes(nameNotes, textNotes, dateCreated, dateEdited, dateReminder);
                    subscriber.onNext(row != NoteDAOImpl.SQL_ERROR);
                    subscriber.onCompleted();
                } catch (Exception exception) {
                    subscriber.onError(exception);
                }
            }
        }).observeOn(AndroidSchedulers.mainThread())
          .subscribeOn(Schedulers.newThread());
    }

    public Observable<Boolean> editNoteById(long id, String nameNotes, String textNotes, long dateEdited, long dateReminder){
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    long row = notesService.editNoteById(nameNotes, textNotes, id, dateEdited, dateReminder);
                    subscriber.onNext(row != NoteDAOImpl.SQL_ERROR );
                    subscriber.onCompleted();
                }
                catch (Exception exception) {
                    subscriber.onError(exception);
                }
            }
        }).observeOn(AndroidSchedulers.mainThread())
          .subscribeOn(Schedulers.newThread());
    }

    public void closeSQLite(){
        notesService.close();
    }

    public Observable<Boolean> deleteNoteById(long id) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    long row = notesService.deleteNoteById(id);
                    subscriber.onNext(row != NoteDAOImpl.SQL_ERROR);
                    subscriber.onCompleted();
                }
                catch (Exception exception) {
                    subscriber.onError(exception);
                }
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread());
    }
}
