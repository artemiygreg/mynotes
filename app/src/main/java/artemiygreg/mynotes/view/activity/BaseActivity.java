package artemiygreg.mynotes.view.activity;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import artemiygreg.mynotes.R;

/**
 * Created by artem_mobile_dev on 27.12.2015.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected static final Handler MAIN_THREAD_HANDLER = new Handler(Looper.getMainLooper());

    @Nullable
    private Toolbar toolbar;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        setupToolbar();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        setupToolbar();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        setupToolbar();
    }

    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    protected void showButtonBack(boolean showButton){
        if(showButton){
            ActionBar actionBar = getSupportActionBar();
            if(actionBar != null){
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener((v)-> onBackPressed());
            }
        }
    }

    @Nullable
    protected Toolbar toolbar() {
        return toolbar;
    }

    protected void showToast(@NonNull String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected void showToast(@StringRes int resId){
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
    }

    protected void runOnUIThread(@NonNull Runnable runnable){
        if (Looper.myLooper() == Looper.getMainLooper()) {
            runnable.run();
        } else {
            MAIN_THREAD_HANDLER.post(runnable);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MAIN_THREAD_HANDLER.removeCallbacksAndMessages(null);
    }
}