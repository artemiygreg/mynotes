package artemiygreg.mynotes.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import artemiygreg.mynotes.R;
import artemiygreg.mynotes.model.Note;
import artemiygreg.mynotes.mvp.models.MyNotesModel;
import artemiygreg.mynotes.mvp.presenters.AddNotePresenter;
import artemiygreg.mynotes.mvp.views.AddOrEditNoteView;
import artemiygreg.mynotes.service.DateService;
import artemiygreg.mynotes.service.NotesServiceImpl;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AddNoteActivity extends BaseActivity implements AddOrEditNoteView {
    private static final int REQUEST_CODE = 1;
    private long dateReminderMS = 0;
    private long timeReminderMS = 0;
    private long dateCreatedNoteMS;
    private AddNotePresenter addNotePresenter;

    @Bind(R.id.editTextNameNote)
    EditText editTextNameNote;

    @Bind(R.id.editTextNote)
    EditText editTextNote;

    @Bind(R.id.textViewDateCreated)
    TextView textViewDateCreated;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_notes_activity);
        showButtonBack(true);
        ButterKnife.bind(this);
        MyNotesModel myNotesModel = new MyNotesModel(new NotesServiceImpl(this));
        addNotePresenter = new AddNotePresenter(myNotesModel);
        addNotePresenter.bindView(this);
        addNotePresenter.onCreate(savedInstanceState, getIntent(), dateCreatedNoteMS = System.currentTimeMillis());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_add_note, menu);
        /**
        * TODO make menu for reminder
        */
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        /**
        * TODO processing click on item
         * @see #onCreateOptionsMenu
        */
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE){
            if(resultCode == RESULT_OK){
                dateReminderMS = data.getLongExtra("date", 0);
                timeReminderMS = data.getLongExtra("time", 0);
                if(dateReminderMS != 0 && timeReminderMS != 0){
                    /*imageViewReminder.setImageDrawable(getResources().getDrawable(R.drawable.ic_alarm_blue_24dp));
                    textViewDateReminder.setText(DateService.convertTimestampToString(dateReminderMS + timeReminderMS,
                            DateService.DATE_AND_TIME_FORMAT_RU));*/
                }
                else {
                   /* imageViewReminder.setImageDrawable(getResources().getDrawable(R.drawable.ic_alarm_grey_24dp));
                    textViewDateReminder.setText(getString(R.string.noSelect));*/
                }
            }
        }
    }

    @OnClick(R.id.floatingActionButtonSave)
    void onClickSaveNote(View view){
        saveNote(editTextNameNote.getText().toString());
    }

    /**
     * TODO select date reminder
     *
     * @OnClick(R.id.buttonCancel)
     * void onClickSelectDateReminder(){
     *     selectDateAndTimeReminder();
     * }
     **/


    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        addNotePresenter.unbindView(this);
        super.onDestroy();
    }

    public void saveNote(String name){
        String text = editTextNote.getText().toString();

        Note note = new Note();
        note.setTitleNotes(TextUtils.isEmpty(name) ? text : name);
        note.setTextNotes(text);
        note.setDateCreated(dateCreatedNoteMS);
        note.setDateEdited(0);
        note.setDateReminder(dateReminderMS + timeReminderMS);

        addNotePresenter.saveNote(note);
    }

    @Override
    public void onCreated(@Nullable Bundle saveInstance, @Nullable Intent intent, long dateCreatedMs) {
        String dateCreated = DateService.convertTimestampToString(dateCreatedMs, DateService.DATE_AND_TIME_FULL_FORMAT_RU);
        textViewDateCreated.setText(dateCreated);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(@Nullable String message, @NonNull Throwable throwable) {
        runOnUIThread(() -> {
            if(message != null) {
                showToast(message);
            }
        });
    }

    /**
     * This method using for displaying note in EditNoteActivity @see {@link EditNoteActivity}
     **/
    @Override
    public void showNoteContent(@NonNull Note note) {

    }

    @Override
    public void noteSavedSuccessfully() {
        runOnUIThread(() -> {
            showToast(R.string.note_saved_successfully);
            Intent intent = new Intent();
            intent.putExtra("date_reminder", dateReminderMS + timeReminderMS);
            setResult(RESULT_OK, intent);
            finish();
        });
    }



    /** this method Deprecated
    *  TODO make alertDialog with DatePicker
    *  @see DatePicker {@link android.widget.DatePicker}
    *
    *  private void selectDateAndTimeReminder(){
    *     Intent intentReminder = new Intent(AddNoteActivity.this, SelectReminderActivity.class);
    *     startActivityForResult(intentReminder, REQUEST_CODE);
    *  }
    */
}
