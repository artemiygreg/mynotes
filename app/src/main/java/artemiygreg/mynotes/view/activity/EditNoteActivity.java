package artemiygreg.mynotes.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import artemiygreg.mynotes.R;
import artemiygreg.mynotes.model.Note;
import artemiygreg.mynotes.mvp.models.MyNotesModel;
import artemiygreg.mynotes.mvp.presenters.EditNotePresenter;
import artemiygreg.mynotes.mvp.views.AddOrEditNoteView;
import artemiygreg.mynotes.service.DateService;
import artemiygreg.mynotes.service.NotesServiceImpl;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditNoteActivity extends BaseActivity implements AddOrEditNoteView {

    @Bind(R.id.editTextNote)
    EditText editTextNote;

    @Bind(R.id.editTextNameNote)
    EditText editTextNameNote;

    @Bind(R.id.textViewDateEdited)
    TextView textViewDateEdited;

    private EditNotePresenter editNotePresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edite_note_activity);
        showButtonBack(true);
        ButterKnife.bind(this);
        MyNotesModel myNotesModel = new MyNotesModel(new NotesServiceImpl(this));
        editNotePresenter = new EditNotePresenter(myNotesModel);
        editNotePresenter.bindView(this);
        editNotePresenter.onCreate(savedInstanceState, getIntent(), 0);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_add_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.floatingActionButtonSave)
    public void onClickSaveNote(){
        saveEditNote(editTextNameNote.getText().toString());
    }

    public void saveEditNote(String name){
        String text = editTextNote.getText().toString();
        editNotePresenter.editNoteById(name, text);
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onCreated(@Nullable Bundle saveInstance, @Nullable Intent intent, long dateCreatedMs) {
        if(intent != null) {
            Note note = intent.getParcelableExtra("note");
            editTextNameNote.setText(note.getTitleNotes());
            textViewDateEdited.setText(DateService.convertTimestampToString(note.getDateCreated(),
                    DateService.DATE_AND_TIME_FULL_FORMAT_RU));
            editTextNote.setText(note.getTextNotes());
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(@Nullable String message, @NonNull Throwable throwable) {

    }

    @Override
    public void showNoteContent(@NonNull Note note) {

    }

    @Override
    public void noteSavedSuccessfully() {
        showToast(R.string.note_edited_successfully);
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        editNotePresenter.unbindView(this);
        super.onDestroy();
    }
}
