package artemiygreg.mynotes.view.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import artemiygreg.mynotes.R;
import artemiygreg.mynotes.broadcast_receiver.ReminderBroadcastReceiver;
import artemiygreg.mynotes.model.Note;
import artemiygreg.mynotes.service.NotesService;
import artemiygreg.mynotes.service.NotesServiceImpl;
import artemiygreg.mynotes.view.adapter.ListNotesAdapter;

public class MainActivity extends BaseActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout drawerLayout;
    private FragmentManager fragmentManager;
    private RecyclerView recyclerView;
    private NotesService notesService;
    private ListNotesAdapter adapter;
    private static final int REQUEST_ADD_OR_EDIT_NOTE = 100;
    private ReminderBroadcastReceiver reminderBroadcastReceiver;
    private TextView textViewNotNotes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        notesService = new NotesServiceImpl(this);
        fragmentManager = getSupportFragmentManager();

        reminderBroadcastReceiver = new ReminderBroadcastReceiver(this);

        initUI();
    }

    private void initUI(){
        setTitle(getResources().getString(R.string.myNotes));
        FloatingActionButton floatingActionButton = (FloatingActionButton)findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(v -> {
            Intent addNoteIntent = new Intent(MainActivity.this, AddNoteActivity.class);
            startActivityForResult(addNoteIntent, REQUEST_ADD_OR_EDIT_NOTE);
        });

        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        List<Note> listNotes = notesService.getListNotes();

        textViewNotNotes = (TextView)findViewById(R.id.textViewNotNotes);

        adapter = new ListNotesAdapter(listNotes);
        adapter.setOnItemClickListener((view, position, note) -> {
            Intent intent = new Intent(MainActivity.this, EditNoteActivity.class);
            intent.putExtra("note", note);
            startActivityForResult(intent, REQUEST_ADD_OR_EDIT_NOTE);
        });
        recyclerView.setAdapter(adapter);

        showListNotes(listNotes);

        initMenu();
    }

    private void showListNotes(List<Note> listNotes){
        if(!listNotes.isEmpty()) {
            recyclerView.setVisibility(View.VISIBLE);
            textViewNotNotes.setVisibility(View.GONE);
        }
        else {
            recyclerView.setVisibility(View.GONE);
            textViewNotNotes.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == REQUEST_ADD_OR_EDIT_NOTE){
            if(data != null && data.hasExtra("date_reminder") && data.getLongExtra("date_reminder", 0) != 0){
                reminderBroadcastReceiver.setAlarmManager(data.getLongExtra("date_reminder", 0));
            }
            adapter.setData(notesService.getListNotes());
            showListNotes(notesService.getListNotes());
        }
    }

    private void initMenu(){
        drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);

        /**
         *  TODO make header to menu
         *
         *  View header = initHeaderMenu();
         *  navigationView.addHeaderView(header);
         *
         */

        navigationView.setNavigationItemSelectedListener(this);
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,  toolbar(), R.string.empty,  R.string.empty) {

            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu();
            }
        };
        drawerLayout.setDrawerListener(mDrawerToggle);
    }


    private boolean selectDrawerItem(MenuItem menuItem){
        int id = menuItem.getItemId();
        switch (id){
            case R.id.navigation_item_notes:
                break;
            case R.id.navigation_item_reminders:
                break;
            case R.id.navigation_item_settings:
                break;
        }
        menuItem.setChecked(true);
        drawerLayout.closeDrawers();

        return true;
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void showFragment(Fragment fragment){
        fragmentManager.beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START) ||
                drawerLayout.isDrawerOpen(GravityCompat.END))
            drawerLayout.closeDrawers();

        super.onBackPressed();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return selectDrawerItem(item);
    }
}
