package artemiygreg.mynotes.view.alert;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import artemiygreg.mynotes.R;

/**
 * Created by Admin on 01.03.15.
 */
public class MyAlertDialog {
    private Context context;

    public MyAlertDialog(Context context){
        this.context = context;
    }

    public void showAlertDeleteNote(String title, DialogInterface.OnClickListener onClickYesListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(R.string.deleting)
                .setMessage(context.getString(R.string.youReallyWantToDeleteNote) + title)
                .setNegativeButton(R.string.no, null)
                .setPositiveButton(R.string.yes, onClickYesListener)
                .create();
        alertDialog.show();
    }
}
