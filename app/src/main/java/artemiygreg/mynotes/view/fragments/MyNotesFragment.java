package artemiygreg.mynotes.view.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import artemiygreg.mynotes.R;
import artemiygreg.mynotes.model.Note;
import artemiygreg.mynotes.mvp.models.MyNotesModel;
import artemiygreg.mynotes.mvp.presenters.MyNotesPresenter;
import artemiygreg.mynotes.mvp.views.MyListNotesView;
import artemiygreg.mynotes.service.NotesServiceImpl;
import artemiygreg.mynotes.view.activity.AddNoteActivity;
import artemiygreg.mynotes.view.activity.EditNoteActivity;
import artemiygreg.mynotes.view.adapter.DividerItemDecoration;
import artemiygreg.mynotes.view.adapter.ListNotesAdapter;
import artemiygreg.mynotes.view.adapter.OnEmptyListListener;
import artemiygreg.mynotes.view.alert.MyAlertDialog;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;


/**
 * Created by artem_mobile_dev on 04.01.2016.
 */
public class MyNotesFragment extends BaseFragment implements MyListNotesView, OnEmptyListListener,
              ListNotesAdapter.OnItemClickListener, ListNotesAdapter.OnDeleteNoteClickListener{
    private static final int REQUEST_ADD_OR_EDIT_NOTE = 100;

    @Bind(R.id.recyclerViewMyNotes)
    RecyclerView recyclerViewMyNotes;

    @Bind(R.id.layoutNotNotes)
    View layoutNotNotes;

    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    @Bind(R.id.floatingActionButton)
    FloatingActionButton floatingActionButton;

    private ListNotesAdapter listNotesAdapter;
    private MyNotesPresenter myNotesPresenter;
    private MyAlertDialog myAlertDialog;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_notes_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        recyclerViewMyNotes.setLayoutManager(new LinearLayoutManager(getContext(), VERTICAL, false));
        recyclerViewMyNotes.addItemDecoration(new DividerItemDecoration(0, 0));
        listNotesAdapter = new ListNotesAdapter();
        listNotesAdapter.setOnItemClickListener(this);
        listNotesAdapter.setOnEmptyListListener(this);
        listNotesAdapter.setOnDeleteNoteClickListener(this);
        recyclerViewMyNotes.setAdapter(listNotesAdapter);

        myAlertDialog = new MyAlertDialog(getContext());

        MyNotesModel myNotesModel = new MyNotesModel(new NotesServiceImpl(getContext()));
        myNotesPresenter = new MyNotesPresenter(myNotesModel);
        myNotesPresenter.bindView(this);
        myNotesPresenter.loadData();
    }

    @OnClick(R.id.floatingActionButton)
    void onClickAddNote() {
        Intent addNoteIntent = new Intent(getContext(), AddNoteActivity.class);
        startActivityForResult(addNoteIntent, REQUEST_ADD_OR_EDIT_NOTE);
    }

    @Override
    public void showLoading() {
        runOnUiThreadIfFragmentAlive(() -> {
            progressBar.setVisibility(View.VISIBLE);
            recyclerViewMyNotes.setVisibility(View.GONE);
            layoutNotNotes.setVisibility(View.GONE);
        });
    }

    @Override
    public void hideLoading() {
        runOnUiThreadIfFragmentAlive(() -> progressBar.setVisibility(View.GONE));
    }

    @Override
    public void showContent(@NonNull List<Note> listNotes) {
        runOnUiThreadIfFragmentAlive(() -> {
            layoutNotNotes.setVisibility(View.GONE);
            listNotesAdapter.setData(listNotes);
            recyclerViewMyNotes.setVisibility(View.VISIBLE);
        });
    }

    @Override
    public void showError(String message) {
        runOnUiThreadIfFragmentAlive(() -> {

        });
    }

    @Override
    public void showError(@StringRes int resId) {
        runOnUiThreadIfFragmentAlive(() -> showToast(resId));
    }

    @Override
    public void showError(@NonNull Throwable error) {
        runOnUiThreadIfFragmentAlive(() -> {

        });
    }


    @Override
    public void showNotNotes() {
        runOnUiThreadIfFragmentAlive(() -> {
            layoutNotNotes.setVisibility(View.VISIBLE);
            recyclerViewMyNotes.setVisibility(View.GONE);
        });
    }

    @Override
    public void noteSuccessfullyDeleted(int position) {
        runOnUiThreadIfFragmentAlive(() -> listNotesAdapter.deletedNote(position));
    }

    @Override
    public void onEmptyList() {
        showNotNotes();
    }

    @Override
    public void onListNotEmpty(int size) {
        runOnUiThreadIfFragmentAlive(() -> {

        });
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        myNotesPresenter.unbindView(this);
        super.onDestroyView();
    }

    @Override
    public void onItemClick(View view, int position, Note note) {
        Intent intent = new Intent(getContext(), EditNoteActivity.class);
        intent.putExtra("note", note);
        startActivityForResult(intent, REQUEST_ADD_OR_EDIT_NOTE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_ADD_OR_EDIT_NOTE){
            if(data != null && data.hasExtra("date_reminder") && data.getLongExtra("date_reminder", 0) != 0){
//                reminderBroadcastReceiver.setAlarmManager(data.getLongExtra("date_reminder", 0));
            }
            myNotesPresenter.loadData();
        }
    }

    @Override
    public void onDeleteClick(View view, int position, Note note) {
        myAlertDialog.showAlertDeleteNote(note.getTitleNotes(),
                (dialog, which) -> myNotesPresenter.deleteNoteById(note.getId(), position));
    }
}
