package artemiygreg.mynotes.view.adapter;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by artem_mobile_dev on 05.01.2016.
 */
public class MarginItemDecoration extends RecyclerView.ItemDecoration {
    public static final int MARGIN_10_PX = 10;
    private final int leftMarginPx;
    private final int topMarginPx;
    private final int rightMarginPx;
    private final int bottomMarginPx;

    public MarginItemDecoration(int leftMarginPx, int topMarginPx, int rightMarginPx, int bottomMarginPx) {
        this.leftMarginPx = leftMarginPx;
        this.topMarginPx = topMarginPx;
        this.rightMarginPx = rightMarginPx;
        this.bottomMarginPx = bottomMarginPx;
    }

    public MarginItemDecoration(int margin) {
        this.leftMarginPx = margin;
        this.topMarginPx = margin;
        this.rightMarginPx = margin;
        this.bottomMarginPx = margin;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        outRect.left = leftMarginPx;
        outRect.top = topMarginPx;
        outRect.right = rightMarginPx;
        outRect.bottom = bottomMarginPx;
    }
}