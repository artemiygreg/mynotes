package artemiygreg.mynotes.view.adapter;

/**
 * Created by artem_mobile_dev on 05.01.2016.
 */
public interface OnEmptyListListener {
    /**
     * This method calling if list is empty
     * list.isEmpty()
     * for example @see {@link ListNotesAdapter method setData()}
     */
    void onEmptyList();
    /**
     * This method calling if list is not empty
     * @param size number of element in list
     * list.size()
     * for example @see {@link ListNotesAdapter method setData()}
     */
    void onListNotEmpty(int size);

}
