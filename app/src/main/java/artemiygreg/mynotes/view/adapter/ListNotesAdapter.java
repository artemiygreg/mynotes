package artemiygreg.mynotes.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.ArrayList;
import java.util.List;

import artemiygreg.mynotes.R;
import artemiygreg.mynotes.model.Note;
import artemiygreg.mynotes.service.DateService;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 01.03.15.
 */
public class ListNotesAdapter extends RecyclerSwipeAdapter<ListNotesAdapter.ViewHolder> {
    private static final String CREATED = "создано ";
    private static final String EDITED = "отредактировано ";
    private static final String TO_REMIND = "напомнить ";
    private List<Note> listNotes;
    private OnItemClickListener onItemClickListener;
    private OnEmptyListListener onEmptyListListener;
    private OnDeleteNoteClickListener onDeleteNoteClickListener;
    public ListNotesAdapter(){
        this.listNotes = new ArrayList<>();
    }

    public ListNotesAdapter(List<Note> listNotes){
        this.listNotes = new ArrayList<>();
        setData(listNotes);
    }

    public void setOnEmptyListListener(OnEmptyListListener onEmptyListListener) {
        this.onEmptyListListener = onEmptyListListener;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeLayout;
    }

    public void setOnDeleteNoteClickListener(OnDeleteNoteClickListener onDeleteNoteClickListener) {
        this.onDeleteNoteClickListener = onDeleteNoteClickListener;
    }

    public void deletedNote(int position) {
        listNotes.remove(position);
        notifyItemRemoved(position);
        if(listNotes.isEmpty()){
            onEmptyListListener.onEmptyList();
        }
        else {
            onEmptyListListener.onListNotEmpty(listNotes.size());
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.titleNotes)
        TextView textViewTitleNotes;

        @Bind(R.id.bottom)
        View bottom;

        @Bind(R.id.dateCreated)
        TextView textViewDateCreatedOrEdited;

        @Bind(R.id.textViewReminder)
        TextView textViewReminder;

        @Bind(R.id.imageButtonDelete)
        ImageView imageButtonDelete;

        @Bind(R.id.imageButtonEdit)
        ImageView imageButtonEdit;

        @Bind(R.id.swipeLayout)
        SwipeLayout swipeLayout;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            swipeLayout.getSurfaceView().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(onItemClickListener != null) {
                onItemClickListener.onItemClick(v, getAdapterPosition(), getItem(getAdapterPosition()));
            }
        }

        public void bind(Note note){
            textViewTitleNotes.setText(note.getTitleNotes());

            swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
            swipeLayout.addDrag(SwipeLayout.DragEdge.Left, bottom);
            swipeLayout.setLeftSwipeEnabled(false);
            swipeLayout.getSurfaceView().setBackgroundDrawable(itemView.getContext().getResources().getDrawable(R.drawable.bg_list_item));
            imageButtonDelete.setOnClickListener(v -> onDeleteNoteClickListener.onDeleteClick(v, getAdapterPosition(), note));
            imageButtonEdit.setOnClickListener(v -> onItemClickListener.onItemClick(v, getAdapterPosition(), note));

            String dateCreateOrEdit;
            if(note.isEdited()) {
                String dateAndTime = DateService.convertTimestampToString(note.getDateEdited(),
                        DateService.DATE_AND_TIME_FULL_FORMAT_RU);
                dateCreateOrEdit = EDITED + dateAndTime;
            }
            else {
                String dateAndTime = DateService.convertTimestampToString(note.getDateCreated(),
                        DateService.DATE_AND_TIME_FULL_FORMAT_RU);
                dateCreateOrEdit = CREATED + dateAndTime;
            }
            textViewDateCreatedOrEdited.setText(dateCreateOrEdit);
            if(note.getDateReminder() != 0){
                String textReminder = TO_REMIND + DateService.convertTimestampToString(note.getDateReminder(),
                        DateService.DATE_AND_TIME_FULL_FORMAT_RU);
                textViewReminder.setText(textReminder);
            }
            else {
                textViewReminder.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_notes, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final Note note = listNotes.get(position);
        viewHolder.bind(note);
        mItemManger.bindView(viewHolder.itemView, position);
    }

    public void setData(List<Note> listNotes){
        this.listNotes.clear();
        this.listNotes.addAll(listNotes);
        if(this.listNotes.isEmpty()){
            onEmptyListListener.onEmptyList();
        }
        else {
            onEmptyListListener.onListNotEmpty(this.listNotes.size());
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listNotes.size();
    }

    public Note getItem(int position){
        return listNotes.isEmpty() ? null : listNotes.get(position);
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position, Note note);
    }

    public interface OnDeleteNoteClickListener{
        void onDeleteClick(View view, int position, Note note);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
}
