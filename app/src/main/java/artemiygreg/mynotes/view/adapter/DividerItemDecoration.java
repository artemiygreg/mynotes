package artemiygreg.mynotes.view.adapter;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import artemiygreg.mynotes.NoteApplication;
import artemiygreg.mynotes.R;

/**
 * Created by artem_mobile_dev on 05.01.2016.
 */
public class DividerItemDecoration extends RecyclerView.ItemDecoration {
    private Drawable mDivider;

    /*
    * Default value 20 px spaces to drawing left and right
    */
    private int marginLeft = 20;
    private int marginRight = 20;


    public DividerItemDecoration() {
        mDivider = NoteApplication.getContext().getResources().getDrawable(R.drawable.line_divider);
    }

    public DividerItemDecoration(int marginLeft, int marginRight) {
        mDivider = NoteApplication.getContext().getResources().getDrawable(R.drawable.line_divider);
        this.marginLeft = marginLeft;
        this.marginRight = marginRight;
    }


    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left + marginLeft, top, right - marginRight, bottom);
            mDivider.draw(c);
        }
    }


}
