package artemiygreg.mynotes;

import android.app.Application;
import android.content.Context;
import android.os.Handler;

import com.squareup.leakcanary.LeakCanary;

/**
 * Created by artem_mobile_dev on 21.11.2015.
 */
public class NoteApplication extends Application {
    private static volatile Context mContext = null;
    public static Handler applicationHandler;


    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();
        applicationHandler = new Handler(mContext.getMainLooper());
        LeakCanary.install(this);
    }

    public static Context getContext() {
        return mContext;
    }
}
