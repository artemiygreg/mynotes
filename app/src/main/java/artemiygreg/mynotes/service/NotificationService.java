package artemiygreg.mynotes.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import artemiygreg.mynotes.NoteApplication;
import artemiygreg.mynotes.R;
import artemiygreg.mynotes.view.activity.MainActivity;

/**
 * Created by artem_mobile_dev on 21.11.2015.
 */
public class NotificationService {
    private Context context;
    private NotificationManager manager;
    private int lastId = 0;

    private NotificationService(){
        this.context = NoteApplication.getContext();
        manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private static class NotificationServiceHolder {
        private static NotificationService notificationService;

        private static NotificationService instance(){
            if(notificationService == null){
                notificationService = new NotificationService();
            }
            return notificationService;
        }
    }

    public static NotificationService getInstance(){
        return NotificationServiceHolder.instance();
    }

    public int sendNotification(String title, String message, PendingIntent pendingIntent){
        Notification notification = new Notification.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setAutoCancel(true)
                .setTicker(message)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(title)
                .setDefaults(Notification.DEFAULT_ALL)
                .build();

        manager.notify(lastId, notification);

        return lastId++;
    }

    public int sendNotification(String title, String message){
        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Notification notification = new Notification.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setAutoCancel(true)
                .setTicker(message)
                .setContentText(message)
                .setContentIntent(PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT))
                .setWhen(System.currentTimeMillis())
                .setContentTitle(title)
                .setDefaults(Notification.DEFAULT_ALL)
                .build();

        manager.notify(lastId, notification);

        return lastId++;
    }
}
