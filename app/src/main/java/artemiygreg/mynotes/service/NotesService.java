package artemiygreg.mynotes.service;

import java.util.List;

import artemiygreg.mynotes.model.Note;

/**
 * Created by Admin on 22.02.15.
 */
public interface NotesService {

    List<Note> getListNotes();
    long saveNotes(String nameNotes, String textNotes, long dateCreated, long dateEdited, long dateReminder);
    int editNoteById(String nameNotes, String textNotes, long id, long dateEdited, long dateReminder);
    long deleteNoteById(long id);
    void close();
}
