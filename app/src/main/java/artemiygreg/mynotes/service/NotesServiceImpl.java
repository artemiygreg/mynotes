package artemiygreg.mynotes.service;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import artemiygreg.mynotes.dao.NoteDAO;
import artemiygreg.mynotes.dao.NoteDAOImpl;
import artemiygreg.mynotes.model.Note;

/**
 * Created by Admin on 23.02.15.
 */
public class NotesServiceImpl implements NotesService {
    private NoteDAO noteDAO;
    private SQLiteDatabase sqLiteDatabaseRead;
    private SQLiteDatabase sqLiteDatabaseWrite;

    public NotesServiceImpl(Context context){
        noteDAO = new NoteDAOImpl(context);
        sqLiteDatabaseRead = noteDAO.getDatabaseRead();
        sqLiteDatabaseWrite = noteDAO.getDatabaseWrite();
    }

    @Override
    public List<Note> getListNotes() {
        List<Note> list;
        try {
            sqLiteDatabaseRead.beginTransaction();
            list = noteDAO.getListNotes(sqLiteDatabaseRead);
            sqLiteDatabaseRead.setTransactionSuccessful();
        }
        finally {
            sqLiteDatabaseRead.endTransaction();
        }
        return list;
    }

    @Override
    public long saveNotes(String nameNotes, String textNotes, long dateCreated, long dateEdited, long dateReminder) {
        long row = NoteDAOImpl.SQL_ERROR;
        try {
            sqLiteDatabaseWrite.beginTransaction();
            row = noteDAO.saveNotes(sqLiteDatabaseWrite, nameNotes, textNotes, dateCreated, dateEdited, dateReminder);
            sqLiteDatabaseWrite.setTransactionSuccessful();
        }
        finally {
            sqLiteDatabaseWrite.endTransaction();
        }

        return row;
    }

    @Override
    public int editNoteById(String nameNotes, String textNotes, long id, long dateEdited, long dateReminder) {
        int row = (int)NoteDAOImpl.SQL_ERROR;
        try {
            sqLiteDatabaseWrite.beginTransaction();
            row = noteDAO.editNoteById(sqLiteDatabaseWrite, nameNotes, textNotes, id, dateEdited, dateReminder);
            sqLiteDatabaseWrite.setTransactionSuccessful();
        }
        finally {
            sqLiteDatabaseWrite.endTransaction();
        }

        return row;
    }

    @Override
    public long deleteNoteById(long id) {
        int row = (int)NoteDAOImpl.SQL_ERROR;
        try {
            sqLiteDatabaseWrite.beginTransaction();
            row = noteDAO.deleteNoteById(sqLiteDatabaseWrite, id);
            sqLiteDatabaseWrite.setTransactionSuccessful();
        }
        finally {
            sqLiteDatabaseWrite.endTransaction();
        }

        return row;
    }

    @Override
    public void close() {
        if(sqLiteDatabaseRead != null && sqLiteDatabaseRead.isOpen()){
            sqLiteDatabaseRead.close();
            sqLiteDatabaseRead = null;
        }
        if(sqLiteDatabaseWrite != null && sqLiteDatabaseWrite.isOpen()){
            sqLiteDatabaseWrite.close();
            sqLiteDatabaseWrite = null;
        }
    }
}
